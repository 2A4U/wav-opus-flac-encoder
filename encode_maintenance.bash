#!/usr/bin/env bash
writeopusPermissions () {
sudo usermod --groups cdrom --append $USER
sudo chown root.cdrom $(which {cdrdao,cdrecord,growisofs,cdda2wav})
sudo chmod 4710 $(which {cdrdao,cdrecord,cdda2wav})
sudo chmod 750 $(which growisofs)
}
