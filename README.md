# wav-opus-flac-encoder
*encode.sh*, a Bash script, converts **.wav* files to **.opus* or **.flac* files.

## Getting started

1. `git clone https://gitlab.freedesktop.org/2A4U/wav-opus-flac-encoder`

2. If the encode directory is in your home folder then type at the command line, `cd ~/wav-opus ; source encode.sh ; encode help`. Alternatively, give *encode.sh* execute permissions with, `chmod u+x encode.sh` then type at the command line, `./encode.sh encode help`.

3. `encode`

## How to create custom opus track info

### Command Line Overrides

`artist`, `title`, `album`, `genre`, `date` can be used per track like so:

```
encode force track 1 title 'A Song Name'
```
Use `force` if a **.cddb* does not exist.

### Writing custom **.inf* files

1. Create **.wav* files in the temporary directory: `encode noclean`. Note 1: use, `encode noclean force` or `encode noclean fast` to write **.wav* files even if **.cddb* files cannot be detected. Note 2: the default temporary directory is '/dev/shm/audio_CD'.

2. Delete all **.inf* or **.cddb* files from temporary directory: `rm /dev/shm/audio_CD/*.{cddb,inf}`.

3. Create a copy of "*audio_01.inf.template*" found in ~/encode/: `cp audio_01.inf.template audio_01.inf`.

4. Edit "*audio_01.inf*" file or "*audio_01.cddb*" file with the track info for each **.wav* to be converted to **.opus*.

5. Copy the "*audio_01.inf*" or "*audio_01.cddb*" to the temporary **.wav* directory: `cp audio_01.inf /dev/shm/audio_CD`.

6. For each **.wav* file create/name and edit a correspoding **.inf* or **.cddb* file--example: for "*audio_02.wav*" create a "*audio_02.inf*": `cp audio_01.inf.template audio_02.inf`.

7. Final opus write: `encode noclean force`. Note: "force" is not necessary if **.cddb* files exist.


## Issue
Insufficient cdrecord privileges might be solved by running, `source encode_maintenance.sh ; encodePermissions` or, `./encode_maintenance.sh encodePermissions` (note: requires sudo privileges). *encode.sh* should still work even with privilege warnings.

